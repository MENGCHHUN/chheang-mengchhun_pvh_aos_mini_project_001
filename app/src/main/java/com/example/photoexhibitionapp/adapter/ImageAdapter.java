package com.example.photoexhibitionapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.databinding.ImageCardBinding;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

//    private final GalleryInterface galleryInterface;
    private final ArrayList<String> imageList;
    private final Context context;
    private static final java.lang.String TAG = "MenuAdapter";

    public ImageAdapter( ArrayList<String> imageList, Context context) {
        this.imageList = imageList;
        this.context = context;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.image_card, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        Glide.with(context).load(imageList.get(position)).into(ImageViewHolder.binding.imageCard);
        Log.d(TAG, "onBindViewHolder: " + imageList.get(position));
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder{

        @SuppressLint("StaticFieldLeak")
        static ImageCardBinding binding;
        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ImageCardBinding.bind(itemView);
        }


//        static ImageViewHolder from (ViewGroup parent){
//            binding = ImageCardBinding.inflate(LayoutInflater.from(parent.getContext()));
//            return new ImageViewHolder(binding.getRoot());
//        }

//        public ImageView bindImage(){
//           return binding.imageCard;
//        }
    }
}
