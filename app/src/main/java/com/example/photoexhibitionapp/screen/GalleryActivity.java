package com.example.photoexhibitionapp.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.Intent;
import android.os.Bundle;

import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.adapter.GalleryAdapter;
import com.example.photoexhibitionapp.databinding.ActivityGalleryBinding;
import com.example.photoexhibitionapp.model.Gallery;

import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity {

    ActivityGalleryBinding binding;
    ArrayList<Gallery> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery);

        list.add(new Gallery("Yellow Bird","100view","https://cdn.pixabay.com/photo/2020/05/19/05/42/weaver-5189346_1280.jpg"));
        list.add(new Gallery("Blue Bird","900view","https://cdn.pixabay.com/photo/2016/04/25/18/07/halcyon-1352522_1280.jpg"));
        list.add(new Gallery("Red Bird","300view","https://cdn.pixabay.com/photo/2020/07/03/23/45/cardinal-5367851_1280.jpg"));
        list.add(new Gallery("Black Bird","200view","https://cdn.pixabay.com/photo/2013/08/10/02/24/bird-171215_1280.jpg"));
        GalleryAdapter adapter = new GalleryAdapter(list,this);
        binding.imageRecyclerView.setAdapter(adapter);
        binding.imageRecyclerView.setLayoutManager(new GridLayoutManager(this,2));

    }
}