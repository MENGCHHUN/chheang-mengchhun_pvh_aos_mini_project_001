package com.example.photoexhibitionapp.model;

public class Contact {
    private String name;
    private String imgUrl;
    private String phoneNumber;

    public Contact() {
    }

    public Contact(String name, String phoneNumber, String imgUrl) {
        this.name = name;
        this.imgUrl = imgUrl;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phonNumber) {
        this.phoneNumber = phonNumber;
    }
}
