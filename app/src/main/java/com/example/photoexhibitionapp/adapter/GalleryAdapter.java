package com.example.photoexhibitionapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.databinding.ActivityGalleryBinding;
import com.example.photoexhibitionapp.databinding.GalleryImageCardBinding;
import com.example.photoexhibitionapp.databinding.ImageCardBinding;
import com.example.photoexhibitionapp.model.Gallery;

import java.util.ArrayList;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>{
    private static final String TAG = "GalleryAdapter";
    private ArrayList<Gallery> galleryList ;
    private static Context context;

    public GalleryAdapter(ArrayList<Gallery> galleryList, Context context) {
        this.galleryList = galleryList;
        this.context = context;
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return GalleryViewHolder.from(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: ");
        Gallery gallery = galleryList.get(position);;
        holder.binData(gallery);

    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

   
    static class GalleryViewHolder extends RecyclerView.ViewHolder{
        static GalleryImageCardBinding binding;
        public GalleryViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        static GalleryViewHolder from (ViewGroup parent){
            binding = GalleryImageCardBinding.inflate(LayoutInflater.from(parent.getContext()));
            return new GalleryViewHolder(binding.getRoot());
        }

        public void binData(Gallery gallery){
            Glide.with(context).load(gallery.getImageUrl()).into(binding.imageCard);
            binding.txtImageName.setText(gallery.getImageName());
            binding.txtImageView.setText(gallery.getViews());
        }

    }
}
