package com.example.photoexhibitionapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.photoexhibitionapp.databinding.ContactCardBinding;
import com.example.photoexhibitionapp.model.Contact;
import com.example.photoexhibitionapp.model.interfaces.ContacViewInterface;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {
    private ArrayList<Contact> contactList;
    private static Context context;
    private final ContacViewInterface contacViewInterface;

    public ContactAdapter(ArrayList<Contact> contactList, Context context, ContacViewInterface contacViewInterface) {
        this.contactList = contactList;
        this.context = context;
        this.contacViewInterface = contacViewInterface;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ContactViewHolder.from(parent, contacViewInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        holder.bindData(contact);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder{
        static ContactCardBinding binding;
        public ContactViewHolder(@NonNull View itemView, ContacViewInterface contacViewInterface) {
            super(itemView);
            Log.d("TAG", "ContactViewHolder: ");
            binding.btnCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contacViewInterface != null){
                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION){
                            contacViewInterface.onClickItem(pos);
                        }
                    }
                }
            });
        }

        static ContactViewHolder from (ViewGroup parent, ContacViewInterface contacViewInterface){
            binding = ContactCardBinding.inflate(LayoutInflater.from(parent.getContext()));
            return new ContactViewHolder(binding.getRoot(), contacViewInterface);
        }
        public void bindData(Contact contact){
            Glide.with(context).load(contact.getImgUrl()).into(ContactViewHolder.binding.contacProfile);
            binding.contactName.setText(contact.getName());
        }
    }
}
