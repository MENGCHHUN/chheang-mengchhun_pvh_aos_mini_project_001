package com.example.photoexhibitionapp.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RadioGroup;

import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.databinding.ActivityUserBinding;

public class UserActivity extends AppCompatActivity {

    ActivityUserBinding binding;
    String gender;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        binding.rGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rFemale){
                    gender = binding.rFemale.getText().toString();
                }else {
                    gender = binding.rMale.getText().toString();
                }
            }
        });
        binding.btnSave.setOnClickListener(v->{
            String name = binding.inputName.getText().toString();
            String career = binding.inputCareer.getText().toString();
            if (name.length() == 0 ){
                binding.inputName.setError("Empty field");
                binding.inputName.startAnimation(shakeAnimation());
            }
            if (career.length() == 0){
                binding.inputCareer.setError("Empty field");
                binding.inputCareer.startAnimation(shakeAnimation());
            }
            if(gender == null){
                binding.rGenderGroup.startAnimation(shakeAnimation());
            }

            if (name.length() != 0 && career.length() != 0 & gender != null ){
                binding.txtResult.setText(name + "\n" + gender + "\n" + career );
            }
        });
    }

    public TranslateAnimation shakeAnimation() {
        TranslateAnimation shakeAnimate = new TranslateAnimation(0, 2, 0, 0);
        shakeAnimate.setDuration(500);
        shakeAnimate.setInterpolator(new CycleInterpolator(7));
        return shakeAnimate;
    }


}