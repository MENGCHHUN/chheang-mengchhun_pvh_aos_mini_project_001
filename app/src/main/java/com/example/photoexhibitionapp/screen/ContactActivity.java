package com.example.photoexhibitionapp.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.adapter.ContactAdapter;
import com.example.photoexhibitionapp.databinding.ActivityContactBinding;
import com.example.photoexhibitionapp.model.Contact;
import com.example.photoexhibitionapp.model.interfaces.ContacViewInterface;

import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity implements ContacViewInterface {
    ActivityContactBinding binding;
    ArrayList<Contact> contactList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact);
        contactList.add(new Contact("Mengchhun","012934939", "https://forum.xda-developers.com/data/avatars/h/12011/12011729.jpg?1657396730"));
        contactList.add(new Contact("Who am i ?","016923823", "https://cdn-icons-png.flaticon.com/512/186/186313.png"));

        ContactAdapter adapter = new ContactAdapter(contactList, this, this);
        binding.contactCardRecycleView.setAdapter(adapter);
        binding.contactCardRecycleView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onClickItem(int position) {
        Contact contact = contactList.get(position);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact.getPhoneNumber()));
        startActivity(intent);
    }
}