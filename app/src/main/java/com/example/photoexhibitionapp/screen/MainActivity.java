package com.example.photoexhibitionapp.screen;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.adapter.ImageAdapter;
import com.example.photoexhibitionapp.databinding.ActivityMainBinding;
import com.example.photoexhibitionapp.databinding.AlertDialogBinding;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    ActivityMainBinding binding;
    ArrayList<String> viewImageList = new ArrayList<>();
    PopupMenu popupMenu;

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        // ==================== Loop Chip ======================
        String[] chipName = {"Pop music", "Romantic", "Horror", "Opera"};
        for (String s : chipName) {
            Chip chip = new Chip(this);
            chip.setText(s);
            chip.setChipBackgroundColorResource(R.color.white);
            binding.chipGroup.addView(chip);
        }
        // ======================================================


        // ======================== Link Image =================
        viewImageList.add("https://cdn.pixabay.com/photo/2013/07/18/20/26/sea-164989_1280.jpg");
         viewImageList.add("https://cdn.pixabay.com/photo/2023/08/06/10/01/bird-8172597_1280.jpg");
         viewImageList.add("https://cdn.pixabay.com/photo/2013/04/04/12/34/mountains-100367_1280.jpg");
         viewImageList.add("https://cdn.pixabay.com/photo/2017/02/07/16/47/kingfisher-2046453_1280.jpg");
         viewImageList.add("https://cdn.pixabay.com/photo/2016/11/08/05/20/sunset-1807524_1280.jpg");

        ImageAdapter adapter = new ImageAdapter(viewImageList, this);
        binding.imageRecyclerView.setAdapter(adapter);
        binding.imageRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        // ========================================================

        binding.menuGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(intent);
            }
        });

        binding.menuNotebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogBinding binding1 = AlertDialogBinding.inflate(LayoutInflater.from(MainActivity.this));
                binding1.acceptButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_SHORT).show();
                    }
                });
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(MainActivity.this)
                        .setTitle("Notebook Service")
                                .setView(binding1.getRoot());
                Log.d(TAG, "onClick: ");
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        binding.menuContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        });

        binding.menuAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UserActivity.class);
                startActivity(intent);
            }
        });

        binding.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu = new PopupMenu(MainActivity.this, binding.btnMenu);
                popupMenu.getMenuInflater().inflate(R.menu.setting_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String str = (String) item.getTitle();
                        Log.d(TAG, "onMenuItemClick: " + str);
                        if (str.equals("Exit")){
                           finish();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });


        



    }

}