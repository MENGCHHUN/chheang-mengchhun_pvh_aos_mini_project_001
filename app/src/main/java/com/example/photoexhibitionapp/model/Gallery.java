package com.example.photoexhibitionapp.model;

import java.io.Serializable;

public class Gallery {

    private String imageName ;
    private String views;
    private String imageUrl;

    public Gallery() {
    }

    public Gallery(String imageName, String views, String imageUrl) {
        this.imageName = imageName;
        this.views = views;
        this.imageUrl = imageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
